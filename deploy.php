<?php
/**
 * Created by PhpStorm.
 * User: Khanh
 * Date: 8/11/14
 * Time: 2:29 PM
 */
require '.deployer/recipe/common.php';
require '.deployer/recipe/symfony.php';

server('dev.vokuro.local', '127.0.0.1')
    ->setPort(2222)
    ->path('/var/www/vokuro')
    ->user('vagrant', 'vagrant');

set('dbUser', 'vagrant');
set('dbPassword', 'idontknow');
set('dbName', 'vokuro');

set('repository', 'git@bitbucket.org:rualatngua/vokuro.git');
set('writeable_dirs', ['app/cache']);

task('deploy:initDB', function() {
    $dbUser = get('dbUser', 'vagrant');
    $dbPass = get('dbPass', 'idontknow');
    $dbName = get('dbName', 'vokuro');

    $releasePath = env()->getReleasePath();

    run("mysql -u$dbUser -p$dbPass -e \"DROP DATABASE IF EXISTS $dbName \"");
    run("mysql -u$dbUser -p$dbPass -e \"CREATE DATABASE $dbName \"");
    run("mysql -uvagrant -pidontknow $dbName < $releasePath/schemas/vokuro.sql");
})->desc('Initializing database');

task('deploy', [
    'deploy:start',
    'deploy:prepare',
    'deploy:update_code',
    'deploy:vendors',
    'deploy:writeable_dirs',
    'deploy:initDB',
    'deploy:symlink',
    'cleanup',
    'deploy:end'
]);